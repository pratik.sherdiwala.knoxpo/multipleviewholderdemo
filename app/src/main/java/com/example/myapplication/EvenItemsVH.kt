package com.example.myapplication

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class EvenItemsVH(itemView: View, val items: ArrayList<String>) : RecyclerView.ViewHolder(itemView) {


    private val mNameTV = itemView.findViewById<TextView>(R.id.itemTV)

    fun bindEvenItems() {
        mNameTV.text = items.get(adapterPosition)
    }

}