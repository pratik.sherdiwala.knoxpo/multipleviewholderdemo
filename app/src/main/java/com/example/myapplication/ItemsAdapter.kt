package com.example.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.lang.IllegalArgumentException

class ItemsAdapter(private val items: ArrayList<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val ITEM_EVEN: Int = 0
    private val ITEM_ODD: Int = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        when (viewType) {
            ITEM_EVEN -> {
                return EvenItemsVH(
                    LayoutInflater.from(parent.context)
                        .inflate(
                            R.layout.list_even_items
                            , parent,
                            false
                        ),
                    items
                )
            }
            ITEM_ODD -> {
                return OddItemsVH(
                    LayoutInflater.from(parent.context)
                        .inflate(
                            R.layout.list_odd_items
                            , parent,
                            false
                        ),
                    items
                )
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is EvenItemsVH -> holder.bindEvenItems()
            is OddItemsVH -> holder.bindItems()
        }

    }


    override fun getItemViewType(position: Int): Int {
        if (position % 2 == 0) {
            return ITEM_EVEN
        } else {
            return ITEM_ODD
        }
    }
}