package com.example.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    var items = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        itemsRV.layoutManager = LinearLayoutManager(this)
        itemsRV.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL))

        for (i in 1..50) {
            items.add("Item " + i)
        }

        itemsRV.adapter=ItemsAdapter(items)

    }

}