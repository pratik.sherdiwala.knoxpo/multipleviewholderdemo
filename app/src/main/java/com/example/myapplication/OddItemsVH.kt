package com.example.myapplication

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class OddItemsVH(itemView: View, val items: ArrayList<String>) : RecyclerView.ViewHolder(itemView) {

    private val mNameTV = itemView.findViewById<TextView>(R.id.itemTV)

    fun bindItems() {

        mNameTV.text = items.get(adapterPosition)

    }

}